//create function-constructor
function Hamburger(size, stuffing) {
    try {
        if (!size) {
            throw new HamburgerException('Enter the required data')
        }
        if (!stuffing) {
            throw new HamburgerException('Enter the required data')
        }
        if (arguments[0].type !== 'size') {
            throw new HamburgerException('Please enter size of the Hamburger')
        }
        if (arguments[1].type !== 'stuffing') {
            throw new HamburgerException('Please enter stuffing of the Hamburger')
        }
        this.size = size;
        this.stuffing = stuffing;
        this.toppings = [];

    }catch(e){
        console.log(e.name, e.message);
    }
}

//properties(constantas) of class Hamburger
Hamburger.SIZE_SMALL = {
    name: 'SIZE_SMALL',
    price: 50,
    calories: 20,
    type: 'size'
};

Hamburger.SIZE_LARGE = {
    name: 'SIZE_LARGE',
    price: 100,
    calories: 40,
    type: 'size'
};
Hamburger.STUFFING_CHEESE = {
    name: 'STUFFING_CHEESE',
    price: 10,
    calories: 20,
    type: 'stuffing'
};
Hamburger.STUFFING_SALAD = {
    name: 'STUFFING_SALAD',
    price: 20,
    calories: 5,
    type: 'stuffing'
};
Hamburger.STUFFING_POTATO = {
    name: 'STUFFING_POTATO',
    price: 15,
    calories: 10,
    type: 'stuffing'
};
Hamburger.TOPPING_MAYO = {
    name: 'TOPPING_MAYO',
    price: 5,
    calories: 20,
    type: 'topping'
};
Hamburger.TOPPING_SPICE = {
    name: 'TOPPING_SPICE',
    price: 15,
    calories: 0,
    type: 'topping'
};

//calculatePrice method of Hamburger prototype
Hamburger.prototype.calculatePrice = function () {

        let toppingsPrice = 0;

        this.toppings.forEach(function(item) {
            toppingsPrice += item.price;
        });

        return this.size.price + this.stuffing.price + toppingsPrice;
};

//calculateCalories method of Hamburger prototype
Hamburger.prototype.calculateCalories = function () {

        let toppingsCalories = 0;

        this.toppings.forEach(function(item) {
            toppingsCalories += item.calories;
        });

        return this.size.calories + this.stuffing.calories + toppingsCalories;
};

 //addTopping and calculate price and calories method of Hamburger prototype.
// create the Array for the toppings to be stored there

Hamburger.prototype.addTopping = function (topping) {
    try {
        if (this.toppings.includes(topping)) {
            throw new HamburgerException('You cannot add one more portion');
        }

        if (topping === undefined) {
            throw new HamburgerException('You have not entered the topping')
        }

        if (topping.type !== 'topping') {
            throw new HamburgerException('You should enter the topping')
        }

        this.toppings.push(topping);
    }
    catch (e) {
        console.log(e.name, e.message);
    }
};

//removeTopping and calculate price and calories method of Hamburger prototype.
Hamburger.prototype.removeTopping = function (topping) {
    try {
        if (!this.toppings.includes(topping)) {
            throw new HamburgerException('You cannot remove the topping, because its unavailable in your order');
        }

        if (topping === undefined) {
            throw new HamburgerException('You have not entered the topping')
        }

        if (topping.type !== 'topping') {
            throw new HamburgerException('Please enter the topping you want to remove from the order')
        }

        this.toppings = this.toppings.filter(function (item) {
            return item !== topping
        });
    }
    catch (e) {
        console.log(e.name, e.message);
    }
};

//Get the list of toppings
Hamburger.prototype.getToppings = function () {
      return this.toppings.map(function (a) {
                return a.name
      })
};

//Get size of the Hamburger
Hamburger.prototype.getSize = function () {
    return this.size;
};


//Get the stuffing  of the Hamburger
Hamburger.prototype.getStuffing = function () {
    return this.stuffing;
};

//Create the Exception function to catch the Errors in the functions
function HamburgerException (message) {
  this.name = "HamburgerException is ";
  this.message = message;
}

//create new object with some parameters and see its price and calories
let hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
console.log("Price: %f", hamburger.calculatePrice());
console.log("Calories: %f", hamburger.calculateCalories());

//add toppings to the new object hamburger
// hamburger.addTopping(Hamburger.TOPPING_SPICE);
// hamburger.addTopping(Hamburger.TOPPING_SPICE);

//try to duplicate the same topping and trace the changes in price and calories
hamburger.addTopping(Hamburger.TOPPING_SPICE);
console.log("Price with topping: %f", hamburger.calculatePrice());
console.log("Calories with topping: %f", hamburger.calculateCalories());


//try to add wrong-topping and see the error
// hamburger.addTopping(Hamburger.STUFFING_POTATO);

//try to enter nothing into the function
hamburger.addTopping();
hamburger.addTopping(Hamburger.TOPPING_MAYO);
console.log("Price with topping: %f", hamburger.calculatePrice());
console.log("Calories with topping: %f", hamburger.calculateCalories());
//
// //try to remove an empty string
// hamburger.removeTopping();

// hamburger.removeTopping(Hamburger.TOPPING_MAYO);
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
console.log("Price without topping: %f", hamburger.calculatePrice());
console.log("Calories without topping: %f", hamburger.calculateCalories());

//get the array of toppings
console.log(hamburger.getToppings());
console.log("Hamburger has %d toppings", hamburger.getToppings().length);

//get the size of the Hamburger
console.log(hamburger.getSize().name);
console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE);
console.log("Is hamburger small: %s", hamburger.getSize() === Hamburger.SIZE_SMALL);

//get the stuffing of the Hamburger
console.log(hamburger.getStuffing().name);